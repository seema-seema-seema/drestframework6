from django.urls import path
from .views import PostList, PostDetail
from rest_framework.urlpatterns import format_suffix_patterns
# from firstapp import views ,UPER VALI LINE NU EDA V LIKH SAKDE A ASI ede lyi fr niche eda likhna pena path('', views.post_list, name='list'),


urlpatterns = [
    #path('', post_list, name='list'),
    path('all/', PostList.as_view()),
    path('<int:pk>/', PostDetail.as_view()),  # pk de jga id v likh SAKDE
]
urlpatterns = format_suffix_patterns(urlpatterns)
